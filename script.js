//on button press, collect the input & format the input, count the result, and print the output
document.getElementById("countButton").onclick = function () {
  let collected = collectTextInput();
  printCounts(countLetters(collected), "lettersDiv");
  printCounts(countWords(collected), "wordsDiv");
};

//collect and format the text input and return a new string
function collectTextInput() {
  let typedText = document.getElementById("textInput").value;
  typedText = typedText.toLowerCase();
  typedText = typedText.replace(/[^a-z'\s]+/g, "");
  return typedText;
}

//count the letters in a string and return a frequency array
function countLetters(stringParam) {
  const letterCounts = {};
  for (let i = 0; i < stringParam.length; i++) {
    currentLetter = stringParam[i];
    if (letterCounts[currentLetter] === undefined) {
      letterCounts[currentLetter] = 1;
    } else {
      letterCounts[currentLetter]++;
    }
  }
  return letterCounts;
}

//count the words in a string and return a frequency array
function countWords(stringParam) {
  const words = stringParam.split(" ");
  const wordCounts = {};
  for (let i = 0; i < words.length; i++) {
    currentWord = words[i];
    if (wordCounts[currentWord] === undefined) {
      wordCounts[currentWord] = 1;
    } else {
      wordCounts[currentWord]++;
    }
  }
  return wordCounts;
}

//print the counts
function printCounts(arrayParam, outputDiv) {
  for (let item in arrayParam) {
    const span = document.createElement("span");
    const textContent = document.createTextNode(
      '"' + item + '": ' + arrayParam[item] + ", "
    );
    span.appendChild(textContent);
    document.getElementById("" + outputDiv + "").appendChild(span);
  }
}
